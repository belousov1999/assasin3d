﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //Editor
    public Transform target;

    //Private
    private Vector3 _prevPosition;

    //Public Static
    public static CameraController Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _prevPosition = target.position;
    }

    public void UpdateTargetCameraPos()
    {
        var changePos = _prevPosition - target.position;
        var camPos = transform.position;
        camPos.x -= changePos.x;
        camPos.z -= changePos.z;
        transform.position = camPos;
        _prevPosition = target.position;
    }

    void LateUpdate()
    {
        if (_prevPosition == target.position) return;
        UpdateTargetCameraPos();
    }
}
