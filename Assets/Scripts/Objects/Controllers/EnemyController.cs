﻿using Objects.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    //Editor
    public float waitingTime;
    public float ShootDelay;
    public float Idle_movespeed;
    public float Attack_movespeed;
    public List<Transform> waypoints;
    public GameObject Rifle;
    public ParticleSystem RifleShot_particles;
    public ParticleSystem Death_particles;
    public bool alwaysReady;
    [SerializeField] private AI_State State;

    //Private
    private Vector3 nextWaypoint, startPos;
    private NavMeshAgent agent;
    private Animator animator;
    private FieldOfView fow;
    private int way_i;
    private float wait_t, move_t, shoot_t;
    private Vector3 lastTargetPos;
    private bool rifled, _dead;

    //Private Static
    private static List<EnemyController> AliveEnemies = new List<EnemyController>();

    private AI_State AiState
    {
        get => State; set
        {
            if (State == value)
                return;
            if (State == AI_State.Attacking)
                agent.speed = Attack_movespeed;
            else
                agent.speed = Idle_movespeed;
            State = value;
            if (!alwaysReady)
                animator.SetBool("RifleMoving", value == AI_State.Moving);
            animator.SetBool("Aim", value == AI_State.Attacking || alwaysReady);
            // Rifle.SetActive(value == AI_State.Moving || value == AI_State.Attacking)
        }
    }

    private enum AI_State
    {
        None,
        Moving,
        Waiting,
        Attacking
    }

    private void Awake()
    {
        if (waypoints != null && waypoints.Count > 0)
        {
            SetNextWaypoint();
            foreach (var way in waypoints)
                way.gameObject.SetActive(false);
        }
        else
            startPos = transform.position;
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        fow = GetComponent<FieldOfView>();
        var main = RifleShot_particles.main;
        main.playOnAwake = false;

        AliveEnemies.Clear();

        animator.SetBool("RifleMoving", true);
    }

    private void Start()
    {
        EventManager.LevelChangedEvent += OnLevelChanged;
        AliveEnemies.Add(this);

    }

    public void OnDeath()
    {
        if (_dead)
            return;
        if (agent.isActiveAndEnabled)
            agent.isStopped = true;
        CGameManager.OnTargetKill();
        AliveEnemies.Remove(this);
        if (AliveEnemies.Count >= 1)
        {
            AliveEnemies[0].AiState = AI_State.Attacking;
            AliveEnemies[0].lastTargetPos = transform.position;
            AliveEnemies[0].move_t = 0;//agent.SetDestination(transform.position);
        }
        StartCoroutine(ShowParticlesThenDestroy());
        _dead = true;
    }

    IEnumerator ShowParticlesThenDestroy()
    {
        yield return new WaitForSeconds(0.2f);
        Death_particles.transform.SetParent(null);
        Death_particles.gameObject.SetActive(true);
        Destroy(this.gameObject);
    }

    void OnLevelChanged(object _, LevelChangedArgs args)
    {
        EventManager.LevelChangedEvent -= OnLevelChanged;
        StartCoroutine(IncreaseTargetCount());
    }

    IEnumerator IncreaseTargetCount()
    {
        yield return new WaitForSeconds(0.5f);
        CGameManager.TargetsCount++;
    }

    void SetNextWaypoint()
    {
        if (waypoints == null || waypoints.Count == 0)
        {
            nextWaypoint = startPos;
            return;
        }
        nextWaypoint = waypoints[way_i].position;
        way_i++;
        if (way_i >= waypoints.Count)
            way_i = 0;
    }

    private void Update()
    {
        if (GameStateManager.GameState != GameStateEnum.Playing) return;
        if (wait_t > 0 && AiState == AI_State.Waiting)
        {
            wait_t -= Time.deltaTime;
            if (fow.visibleTargets.Count > 0)
            {
                wait_t = 0;
                agent.isStopped = true;
                AiState = AI_State.Attacking;
            }
            return;
        }
        agent.enabled = wait_t <= 0;
        if (agent.path.corners.Length < 2 && move_t >= 1f && !alwaysReady
            || ((waypoints == null || waypoints.Count == 0) && AiState != AI_State.Attacking))
        {
            move_t = 0;
            AiState = AI_State.Waiting;
            SetNextWaypoint();
            wait_t = waitingTime;
            return;
        }
        else if (AiState == AI_State.Attacking || alwaysReady)
        {
            animator.SetBool("Aim", true);
            if (fow.visibleTargets.Count > 0)
            {
                transform.LookAt(fow.visibleTargets[0]);

                if (shoot_t >= 0.4f)
                    rifled = true;
                // if (rifled)
                //     Rifle.SetActive(true);
                lastTargetPos = fow.visibleTargets[0].position;
                shoot_t += Time.deltaTime;
                if (shoot_t >= ShootDelay)
                {
                    shoot_t = 0;
                    animator.SetTrigger("Shoot");
                    RifleShot_particles.Play();
                    PlayerController.Instance.OnTakeDamage();
                }
            }
            else if (!alwaysReady)
            {
                rifled = false;
                shoot_t = 0;
                //  Rifle.SetActive(false);
                move_t += Time.deltaTime;
                nextWaypoint = lastTargetPos;
                AiState = AI_State.Moving;
            }
            else
                AiState = AI_State.Waiting;
        }
        else
            AiState = AI_State.Moving;
        if (agent.enabled)
        {
            move_t += Time.deltaTime;
            if (fow.visibleTargets.Count > 0)
            {
                agent.isStopped = true;
                AiState = AI_State.Attacking;
            }
            else
            {
                agent.isStopped = false;
                agent.SetDestination(nextWaypoint);
            }
        }
    }
}
