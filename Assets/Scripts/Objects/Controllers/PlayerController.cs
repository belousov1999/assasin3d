﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    //Editor
    public int hp;
    public NavMeshAgent agent;
    public LineRenderer line_rend;
    public List<Renderer> mesh_renderers;

    //Private
    private Animator animator;
    private Coroutine follow_coroutine;
    [Header("Test")]
    [SerializeField] private int _currentHp;

    //Public Static
    public static PlayerController Instance { get; private set; }

    private void Start()
    {
        Instance = this;
        animator = GetComponent<Animator>();
        _currentHp = hp;
    }

    public void OnTakeDamage()
    {
        _currentHp--;
        var l = Mathf.Lerp(0, 1, (float)((float)_currentHp / (float)hp));
        foreach (var rend in mesh_renderers)
        {
            var c = rend.material.color;
            c.g = l;
            c.b = l;
            rend.material.SetColor("_Color", c);
        }
        if (_currentHp <= 0)
        {
            GameStateManager.GameState = GameStateEnum.FinishLose;
            agent.isStopped = true;
        }
    }

    void OnNewPath()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            if (follow_coroutine != null)
                StopCoroutine(follow_coroutine);
            
            if (hit.collider.gameObject.CompareTag("Enemy"))
            {
                follow_coroutine = StartCoroutine(FollowEnemy(hit.collider.transform));
                return;
            }
            var pos = hit.collider.transform.position;
            var point = new Vector3(pos.x, hit.point.y, pos.z);
            agent.SetDestination(point);
        }
    }

    IEnumerator FollowEnemy(Transform enemy)
    {
        while (true)
        {
            if (enemy)
            {
                var pos = enemy.position;
                var point = new Vector3(pos.x, pos.y, pos.z);
                agent.SetDestination(point);
                yield return new WaitForSeconds(0.1f);
            }
            else
                break;
        }
    }

    void DrawPath(NavMeshPath path)
    {
        if (path.corners.Length < 2)
        {
            line_rend.positionCount = 0;
            return;
        }
        line_rend.positionCount = path.corners.Length;

        for (var i = 0; i < path.corners.Length; i++)
        {
            line_rend.SetPosition(i, path.corners[i] + Vector3.up);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_currentHp<=0 || !other.gameObject.CompareTag("Enemy")) return;
        var enemy = other.gameObject.GetComponent<EnemyController>();
        if (enemy != null)
        {
            if (follow_coroutine != null)
                StopCoroutine(follow_coroutine);
            animator.SetTrigger("Stab");
            enemy.OnDeath();
            ComboTextManager.IncreaseCombo();
        }
        else
        {
            Debug.LogError("Enemy doesn`t have 'EnemyController' script", other.gameObject);
        }
    }

    void Update()
    {
        animator.SetBool("Moving", agent.path.corners.Length >= 2);

        DrawPath(agent.path);

        if (GameStateManager.GameState != GameStateEnum.Playing) return;

        if (Input.GetMouseButtonDown(0))
            OnNewPath();
        else if (Input.touchCount > 0
            && Input.GetTouch(0).phase == TouchPhase.Began)
            OnNewPath();
    }
}
