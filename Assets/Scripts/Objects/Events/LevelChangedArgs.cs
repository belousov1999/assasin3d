﻿using System;

namespace Objects.Events
{
    public class LevelChangedArgs:EventArgs
    {
        public LevelChangedArgs(string previous, string current)
        {
            Previous = previous;
            Current = current;
        }

        public string Previous { get; private set; }
        public string Current { get; private set; }
    }
}
