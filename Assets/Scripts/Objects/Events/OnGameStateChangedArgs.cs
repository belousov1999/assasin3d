﻿using System;

namespace Objects.Events
{
    public class OnGameStateChangedArgs : EventArgs
    {
        public GameStateEnum PrevGameState { get; private set; }
        public GameStateEnum NewGameState { get; private set; }

        public OnGameStateChangedArgs(GameStateEnum prevGameState, GameStateEnum newGameState)
        {
            PrevGameState = prevGameState;
            NewGameState = newGameState;
        }
    }
}
