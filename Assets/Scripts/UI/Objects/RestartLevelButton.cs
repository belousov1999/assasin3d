﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartLevelButton : UIObject
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            LevelManager.RestartLevel();
        });
    }
}
