﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextLevelButton : UIObject
{
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            LevelManager.SetupNextLevel();
        });
    }

    public override bool IsReady()
    {
        return LevelManager.HasNextLevel();
    }
}
