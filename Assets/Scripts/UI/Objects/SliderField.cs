﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderField : MonoBehaviour
{
    //Editor
    public Slider slider;

    //Protected
    protected float value;
    protected float Value
    {
        get => value; set
        {
            if (this.value != value)
            {
                inputField.text = value + "";
                slider.value = value;
                this.value = value;
                OnValueChanged(value);
            }
        }
    }
    protected InputField inputField;

   protected void Start()
    {

        inputField = GetComponent<InputField>();

        inputField.text = Value + "";

        inputField.onEndEdit.AddListener((s) =>
        {
            if (float.TryParse(s, out float res_f))
            {
                if (res_f > slider.maxValue)
                    res_f = slider.maxValue;
                Value = res_f;
            }
            else
                inputField.text = Value + "";
        });

        slider.onValueChanged.AddListener((v) => Value = v);
    }

    protected virtual void OnValueChanged(float f){ }
}
