﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Objects
{
    public class ProgressBar : MonoBehaviour
    {
        //Editor
        public Image progressImage;
        public float maxValue;
        public Text text;

        //Private
        private float _currentValue;

        //Public
        public float Value
        {
            get => _currentValue; set
            {
                var v = value > maxValue ? maxValue : value;
                progressImage.fillAmount = Mathf.Clamp01(v);
                _currentValue = v;
                if (text != null)
                {
                    text.text = (int)(Mathf.Clamp01(v) * 100) + "%";
                }
            }
        }
    }
}