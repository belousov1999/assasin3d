﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.Enums;

namespace UI.Objects
{
    public class SpecialObject
    {
        public UiTypeEnum uiType;
        public bool showing;

        public SpecialObject(UiTypeEnum uiType, bool showing)
        {
            this.uiType = uiType;
            this.showing = showing;
        }
    }
}
