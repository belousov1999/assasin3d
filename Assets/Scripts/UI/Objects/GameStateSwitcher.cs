﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateSwitcher : MonoBehaviour
{
    //Editor
    public GameStateEnum switchTo;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(()=> {
            GameStateManager.GameState = switchTo;
        });    
    }
}
