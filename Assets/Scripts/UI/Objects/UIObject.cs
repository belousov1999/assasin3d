﻿using System.Collections;
using System.Collections.Generic;
using UI.Enums;
using UnityEngine;

public class UIObject : MonoBehaviour
{
    //Editor
    public UiTypeEnum uiType;
    public List<GameStateEnum> gameStates;

    public virtual bool IsReady() 
        => true; 
}
