﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Objects
{
    public class LoadingMenu : UIObject
    {
        //Editor
        public ProgressBar progressBar;

        //Private Static
        private static LoadingMenu _instance;

        void Awake()
        {
            _instance = this;
        }

        public static void SetLoadingLevelProgress(float progress)
        {
            if(_instance == null)
            {
                Debug.LogError("[LoadingMenu]: LoadingMenu not preloaded. Did you turn on the script? ");
                return;
            }
            _instance.progressBar.Value = progress;
        }
        
    }
}