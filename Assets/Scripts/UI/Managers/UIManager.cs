﻿using Objects.Events;
using System.Collections;
using System.Collections.Generic;
using UI.Enums;
using UI.Objects;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    //Editor
    public List<UIObject> uiObjectsList;

    //Private Static
    private static UIManager _instance;

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this;
        EventManager.OnGameStateChangedEvent += OnGameStateChanged;
        DontDestroyOnLoad(this.gameObject);
    }

    private void OnDestroy()
    {
        EventManager.OnGameStateChangedEvent -= OnGameStateChanged;
    }

    public static void ShowSpecial(List<SpecialObject> specialList)
    {
        foreach (var uiObj in _instance.uiObjectsList)
        {
            if (uiObj == null) continue;
            foreach (var kv in specialList)
                if (uiObj.uiType == kv.uiType)
                {
                    uiObj.gameObject.SetActive(kv.showing);
                    break;
                }
        }
    }

    public static T GetSpecialObject<T>(UiTypeEnum type) where T : class
    {
        if (_instance == null) return null;
        foreach (var uiObj in _instance.uiObjectsList)
            if (uiObj.uiType == type)
                return uiObj.gameObject.GetComponent<T>();
        return null;
    }

    void OnGameStateChanged(object _, OnGameStateChangedArgs args)
    {
        foreach (var obj in uiObjectsList)
        {
            if (obj == null) continue;
            var doActive = false;
            foreach (var state in obj.gameStates)
            {
                if (state == args.NewGameState)
                {
                    if (obj.IsReady())
                        doActive = true;
                    break;
                }
            }
            obj.gameObject.SetActive(doActive);
        }
    }


}
