﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboTextManager : MonoBehaviour
{
    //Editor
    public Text combo_text;
    public float comboDelay;

    //Private Static
    private static ComboTextManager _instance;

    //Private
    private float combo_t;
    private int combo;

    private void Awake()
    {
        _instance = this;
        EventManager.LevelChangedEvent += (_, args) =>
        {
            _instance.combo_text.gameObject.SetActive(false);
        };
    }

    public static void IncreaseCombo()
    {
        _instance.combo++;
        _instance.combo_t = _instance.comboDelay;
        _instance.combo_text.gameObject.SetActive(_instance.combo >= 2);
        if (_instance.combo >= 2)
        {
            _instance.combo_text.text = "COMBO\n" + _instance.combo + "x";
        }
    }

    private void Update()
    {
        if (combo_t > 0)
        {
            combo_t -= Time.deltaTime;
            if (combo_t <= 0)
            {
                combo_text.gameObject.SetActive(false);
                combo = 0;
            }
        }
    }
}
