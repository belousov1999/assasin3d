﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.Enums;
using UI.Objects;

namespace UI.Managers
{
    public static class HoldTextManager
    {
        //Private Static
        private static bool isHoldText;
        private static SpecialObject hold_text, unhold_text;
        private static List<SpecialObject> sendList;

        static HoldTextManager()
        {
            isHoldText = true;
            hold_text = new SpecialObject(UiTypeEnum.HoldText, true);
            unhold_text = new SpecialObject(UiTypeEnum.UnholdText, false);
            sendList = new List<SpecialObject>() {
                hold_text,
                unhold_text
            };
        }

        public static void UpdateShowing(bool isUnhold)
        {
           
            if (isHoldText == isUnhold) return;
            isHoldText = isUnhold;
            UnityEngine.Debug.Log("Holding: " + isUnhold);

            hold_text.showing = !isUnhold;
            unhold_text.showing = isUnhold;
            UIManager.ShowSpecial(sendList);
        }
    }
}
