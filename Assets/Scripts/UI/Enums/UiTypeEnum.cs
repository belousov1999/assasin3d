﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Enums
{
    public enum UiTypeEnum
    {
        NonSpecial,
        MainMenu,
        LoadingMenu,
        GameMenu,
        NextLevelButton,
        HoldText,
        UnholdText,
        RaceTracker,
        RacePlaceText
    }
}