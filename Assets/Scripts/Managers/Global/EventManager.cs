﻿using Objects.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    //Events
    public static EventHandler<OnGameStateChangedArgs> OnGameStateChangedEvent = null;
    public static EventHandler<LevelChangedArgs> LevelChangedEvent = null;

    //Invokers
    public static void OnGameStateChanged(GameStateEnum prev, GameStateEnum curr) => OnGameStateChangedEvent?.Invoke(null, new OnGameStateChangedArgs(prev, curr));
    public static void OnLevelChanged(string prev, string curr) => LevelChangedEvent?.Invoke(null, new LevelChangedArgs(prev, curr));
}
