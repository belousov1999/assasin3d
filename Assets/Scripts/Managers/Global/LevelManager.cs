﻿using System.Collections;
using System.Collections.Generic;
using UI.Objects;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    //Editor
    public int LevelsCount;

    //Private
    private int _currentLevel = 0;

    //Private Static
    private static LevelManager _instance;
    private static readonly string LevelPrefix = "Level_";

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
    }

    void Start()
    {
        StartCoroutine(LoadLevelInner(LevelPrefix + _currentLevel)); //Setup zero level
    }

    public static bool HasNextLevel()
        => _instance.LevelsCount > _instance._currentLevel + 1;

#if UNITY_EDITOR
    [UnityEditor.MenuItem("Custom/Setup Next Level")]
#endif
    public static bool SetupNextLevel()
    {
        if (!HasNextLevel()) return false;
        _instance._currentLevel++;
        _instance.StartCoroutine(_instance.LoadLevelInner(LevelPrefix + _instance._currentLevel));
        return true;
    }

    public static void RestartLevel()
        => _instance.StartCoroutine(_instance.LoadLevelInner(LevelPrefix + _instance._currentLevel));

    public void SetupNextLevel_forButtons()
        => SetupNextLevel();

    private IEnumerator LoadLevelInner(string levelName)
    {
        GameStateManager.GameState = GameStateEnum.Loading;
        var prevScene = SceneManager.GetActiveScene().name;
        AsyncOperation async = SceneManager.LoadSceneAsync(levelName);
        while (!async.isDone)
        {
            LoadingMenu.SetLoadingLevelProgress(async.progress);
            yield return null;
        }
        if (async.isDone)
        {
            yield return new WaitForSeconds(1f);
            EventManager.OnLevelChanged(prevScene, levelName);
            // GameStateManager.GameState = Managers.Enums.GameStateEnum.GameMenu;
            GameStateManager.GameState = GameStateEnum.Start;
        }
    }
}
