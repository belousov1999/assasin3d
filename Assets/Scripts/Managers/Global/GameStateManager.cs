﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    //Private Static
    private static bool _inited;
    private static GameStateEnum gameState;

    //Public Static
    public static GameStateEnum GameState
    {
        get => gameState; set
        {
            EventManager.OnGameStateChanged(gameState, value);
            Debug.LogFormat("[GameStateManager] GameStateChanged: {0} => {1}", gameState, value);
            gameState = value;
        }
    }

    void Start()
    {
        if(_inited)
        {
            Destroy(this.gameObject);
            return;
        }
        _inited = true;
        GameState = GameStateEnum.Loading;
        DontDestroyOnLoad(this.gameObject);
    }
}
