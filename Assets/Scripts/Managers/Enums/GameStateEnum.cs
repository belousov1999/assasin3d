﻿
public enum GameStateEnum
{
    GameMenu,
    Start,
    Playing,
    FinishWin,
    FinishLose,
    Loading,
}
