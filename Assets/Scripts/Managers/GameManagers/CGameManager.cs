﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CGameManager : MonoBehaviour
{
    //Editor
    public Text t_killsCounter;

    //Private Static
    private static CGameManager _instance;
    private static int targetsCount;

    //Public Static
    public static int TargetsCount { get => targetsCount; set {


            targetsCount = value;
            _instance.t_killsCounter.text = TargetsKills+"/" + TargetsCount;
        }
    }
    public static int TargetsKills { get; set; }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this;
        EventManager.LevelChangedEvent += (_, __) => { TargetsCount = 0; TargetsKills = 0; };
    }

  

    public static void OnTargetKill()
    {
        TargetsKills++;
        _instance.t_killsCounter.text = TargetsKills + "/" + TargetsCount;
        if (TargetsKills >= TargetsCount)
            GameStateManager.GameState = GameStateEnum.FinishWin;
    }
}
